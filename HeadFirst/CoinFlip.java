public class CoinFlip
{
    public static void main (String[] args)
    {
        int a = (int) (Math.random() * (2));
        
        if (a == 1)
        {
            System.out.println("Heads");
        }
        else
        {
            System.out.println("Tails");
        }
    }
}

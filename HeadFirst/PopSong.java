public class PopSong
{
    public static void main (String[] args) 
    {
        int popNum = 99;
        String word = "bottles";

        while (popNum > 0) 
        {
            if (popNum == 1) 
            {
                word = "bottle";
            }
            System.out.println(popNum + " " + word + " of pop on the wall");
            System.out.println(popNum + " " + word + " of pop.");
            System.out.println("Take one down.");
            System.out.println("Pass it around.");
            popNum = popNum - 1;

            if (popNum > 1) 
            {
                System.out.println (popNum + " " + word + " of pop on the wall");
                
            }
            else if (popNum == 1) 
            {
                System.out.println (popNum + " bottle of pop on the wall");
            }
            else 
            {
                System.out.println("No more bottles of pop on the wall");
            }
        }
    }
}

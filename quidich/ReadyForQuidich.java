import java.util.Scanner;

class ReadyForQuidich 
{
	public static void main (String[] args)
	{
		String[] Houses = {"Gryffindor", "Hufflepuff", "Ravenclaw", "Slytherin"};

		int gryff = (0);
		int huff = (0);
		int rave = (0);
		int sly = (0);

		Scanner scanner = new Scanner(System.in);

		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			
			if (line.contains("Gryffindor"))
			{
				gryff = (gryff + 1);
			}
			
			else if (line.contains("Hufflepuff"))
			{
				huff = (huff +1);
			}
			
			else if (line.contains("Ravenclaw"))
			{
				rave = (rave + 1);
			}

			else if (line.contains("Slytherin"))
			{
				sly = (sly + 1);
			}


		}
		
		if (gryff != 7 || huff != 7 || rave != 7 || sly != 7)
		{
			double[] HousesNum = {gryff, huff, rave, sly};
			for (int i = 0; i < 4; i++)
			{
				if (HousesNum[i] > 7)
				{
					System.out.println(Houses[i] + " has too many players");
				}
				else 
				{
					System.out.println(Houses[i] + " does not have enough players");
				}
			}

		}
		else 
		{
		System.out.println ("List complete, let's play quidditch!");
		}	
	}

}

import java.awt.*;
import javax.swing.*;



public class Drawing
{
	public static void main (String[] args)
	{
		Drawing gui = new Drawing();
		gui.go();
	}

	public void go()
	{
		JFrame frame = new JFrame();
		MyDrawPanel panel = new MyDrawPanel();
		
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setSize(300,300);
		frame.add (panel);
		frame.setVisible(true);
	}
}

class MyDrawPanel extends JPanel
{
	public void paintComponent(Graphics g)
	{
		Image image = new ImageIcon("return.jpg").getImage();
		g.drawImage(image,3,4,this);
	}
}
